package advanced_jpa.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Entity
@Table (name = "prices")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Price {

    @Id
    private Long id;
    private BigDecimal priceOut;
    private BigDecimal priceIn;

    @ManyToMany(mappedBy = "prices")
    private List<Product> products;
}
