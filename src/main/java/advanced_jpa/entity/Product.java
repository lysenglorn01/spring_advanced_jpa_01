package advanced_jpa.entity;

import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "products")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "product_name", length = 100)
    private String productName;

    @Column(columnDefinition = "text")
    private String description;

    @ManyToOne
    @JoinColumn(name = "cate_id")
    private Category category;

    @ManyToMany
    @JoinTable(name = "product_prices",
    joinColumns = @JoinColumn(name = "product_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "price_id", referencedColumnName = "id"))
    private List<Price> prices;

    @OneToMany(mappedBy = "product")
    private List<OrderDetail> orderDetails;




}
