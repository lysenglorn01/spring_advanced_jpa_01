package advanced_jpa.initialize;

import advanced_jpa.entity.Product;
import advanced_jpa.repository.ProductRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DataInitialization {
    private final ProductRepository productRepository;

    @PostConstruct
    /*
     * @PostConstruct is annotation use for make method work when application stat
     */
    void init() {
        System.out.println("Start initialize data...");

        Product product1 = Product.builder()
                .productName("Iphone 15 Pro Mix")
                .description("Iphone company")
                .build();
        Product product2 = Product.builder()
                .productName("MixBook Pro")
                .description("Iphone company")
                .build();
        productRepository.save(product1);
        productRepository.save(product2);

        List<Product> products = productRepository.findAll();
        products.forEach(product -> System.out.println(product.getProductName()));

    }
}
